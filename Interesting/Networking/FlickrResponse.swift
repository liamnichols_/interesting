//
//  FlickrResponse.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import Foundation

struct FlickrResponse {
    
    /// The single date formatter used for parsing flickr response values
    static let dateFormatter: DateFormatter = DateFormatter.flickrDateFormatter()
    
    /// The parsed data
    let data: [String: Any]
    
    /// Attempts to parse the response from an API call with the supplied parameters or throws an error describing the failure
    ///
    /// - Parameters:
    ///   - data: The response data returned in the HTTP response
    ///   - response: The URLResponse object returned from the data task
    ///   - error: The error returned from the data task if any
    /// - Throws: A FlickrError if the data could not be parsed
    init(data: Data?, response: URLResponse?, error: Error?) throws {
        
        /* Response Checks:
         * ================
         *
         *  - error == nil
         *  - response is HTTPURLResponse
         *  - response statusCode == 200
         *  - data != nil
         *  - data parses as JSON
         *  - data is [String:Any]
         *  - data["stat"] == "ok"
         */
        
        // Check for an NSURLError
        if error != nil {
            throw FlickrError(code: .networkingError)
        }
        
        // Ensure that we got a 200 status code
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            throw FlickrError(code: .unknownServerError)
        }
        
        // Make sure the data is there
        guard let data = data else {
            throw FlickrError(code: .unknownServerError)
        }
        
        // Parse the data
        guard let jsonObject = try? JSONSerialization.jsonObject(with: data, options: [ ]), let json = jsonObject as? [String: Any] else {
            throw FlickrError(code: .parsingError)
        }
        
        // Make sure we got the stat back
        guard let stat = json["stat"] as? String, stat == "ok" else {
            throw FlickrError(code: .serverError)
        }
        
        // Parsing was successful
        self.data = json
    }
    
    /// Attempts to parse a "photos" response from the API.
    ///
    /// - Returns: A FlickrPhotos object 
    /// - Throws: A FlickrError if the response is incorrectly strucutred
    func getPhotos() throws -> FlickrPhotos {
        return try FlickrPhotos(data: data)
    }
}

// MARK: - Date Formatting
private extension DateFormatter {
    
    static func flickrDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX") // always use a machine locale!
        formatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ss"
        return formatter
    }
}
