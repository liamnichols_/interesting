//
//  FlickrPhotos.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import Foundation

/// An individual photo formed from the response of the FlickrAPI
struct FlickrPhoto {
    
    let identifier: String
    let owner: String
    let secret: String
    let server: String
    let farm: Int
    let title: String
    let placeId: String?
    let tags: String
    let imageZ: (url: URL, size: (width: Double, height: Double))?
    let dateTaken: Date?
    
    init(data: [String: Any]) throws {
        
        // Attempt to get everything
        guard let identifier = data["id"] as? String,
            let owner = data["owner"] as? String,
            let secret = data["secret"] as? String,
            let server = data["server"] as? String,
            let farm = data["farm"] as? Int else {
                throw FlickrError(code: .parsingError)
        }
        
        // Set everything
        self.identifier = identifier
        self.owner = owner
        self.secret = secret
        self.server = server
        self.farm = farm
        self.title = data["title"] as? String ?? ""
        self.placeId = data["place_id"] as? String
        self.tags = data["tags"] as? String ?? ""
        
        // Try to parse the imageZ as its optional
        if let urlZString = data["url_z"] as? String,
            let urlZ = URL(string: urlZString),
            let heightZ = data["height_z"] as? String,
            let widthZ = data["width_z"] as? String,
            let height = Double(heightZ),
            let width = Double(widthZ),
            width > 0.0 && height > 0.0 {
            
            self.imageZ = (url: urlZ, (width: width, height: height))
        } else {
            self.imageZ = nil
        }
        
        // Try to parse the date taken
        if let dateTaken = data["datetaken"] as? String, let date = FlickrResponse.dateFormatter.date(from: dateTaken) {
            self.dateTaken = date
        } else {
            self.dateTaken = nil
        }
    }
}

/// A paged response of photos returned from the FlickrAPI
struct FlickrPhotos {
    
    let page: Int
    let pages: Int
    let perPage: Int
    let total: Int
    let photo: [FlickrPhoto]
    
    init(data: [String: Any]) throws {
        
        // Make sure the photos object is there
        guard let photos = data["photos"] as? [String: Any] else {
            throw FlickrError(code: .parsingError)
        }
        
        // Attempt to get everything else
        guard let page = photos["page"] as? Int,
            let pages = photos["pages"] as? Int,
            let perPage = photos["perpage"] as? Int,
            let total = photos["total"] as? Int,
            let photo = photos["photo"] as? [[String: Any]] else {
                throw FlickrError(code: .parsingError)
        }
        
        // Set everything
        self.page = page
        self.pages = pages
        self.perPage = perPage
        self.total = total
        self.photo = try photo.map(FlickrPhoto.init(data:))
    }
}
