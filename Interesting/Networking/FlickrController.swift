//
//  FlickrController.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import Foundation

/// The API key used to authenticate with the Flickr API
private let flickrAPIKey = "aac27e61031338b8c1fbd6919f56e8fe"

/// Protocol for identifying Flickr API methods
protocol FlickrMethod {
    
    /// The name of the Flickr API method to pass into the `method` query parameter
    var name: String { get }
}

// MARK: - FlickrController
/// A controller object responsible for communicating with the Flickr API
class FlickrController {
    
    /// The URLSession used for making API calls
    let session: URLSession
    
    init() {
        
        // Create a default session
        self.session = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    /// Executes a GET request against the Flickr API with the specified method and arguments. 
    /// More info about the Flickr API itself can be found here: https://www.flickr.com/services/api/
    ///
    /// - Parameters:
    ///   - method: The API method to execute. See https://www.flickr.com/services/api/
    ///   - arguments: The arguments to pass into the request. See API documentation for the specific method for prarameters available to that method
    ///   - completion: The completion block executed upon success or failure. Note that this block is called on a background thread.
    func get(from method: FlickrMethod,
             with arguments: [Flickr.Argument] = [],
             completion: @escaping (Result<FlickrResponse>) -> Swift.Void) {
        
        // Build the url with the base components, method and additional arguments
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.flickr.com"
        components.path = "/services/rest"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: flickrAPIKey),
            URLQueryItem(name: "format", value: "json"),
            URLQueryItem(name: "nojsoncallback", value: "1"),
            URLQueryItem(name: "method", value: method.name)
        ] + arguments.map({ $0.queryItem })
        
        // Get the URL. This would only fail to unwrap here if i messed up the above, no user input can effect this so lets force unwrap
        // swiftlint:disable force_cast
        let url = components.url!
        
        // Build the task
        let task = session.dataTask(with: url) { data, response, error in
            
            // Callback with either the response or the error
            do {
                completion(.success(try FlickrResponse(data: data, response: response, error: error))) // FlickrResponse handles parsing
            } catch {
                completion(.failure(error))
            }
        }
        
        // Start the task
        task.resume()
        
        // Note: Might want to return the URLSessionTask in the future to allow for cancelling if we want to abort?
    }
}

/// An object to represent errors that occur while communicating with the FlickrAPI
struct FlickrError: Error {
    
    /// A code describing the type of error that took place
    ///
    /// - networkingError: There was an error executing the session task. Likely to be device connection issues (no internet, unknown host etc)
    /// - unknownServerError: The server didn't respond correctly. Likely to be an error with arguments parsed or the server is down (i.e HTTP 500 errors)
    /// - parsingError: There was an error parsing the response. This could be the format (i.e no JSON) or required parameters were missing
    /// - serverError: The server didn't return "ok" in the "stat" response. See https://www.flickr.com/services/api/response.json.html
    enum Code {
        case networkingError
        case unknownServerError
        case parsingError
        case serverError
    }
    
    /// The error code describing the cause of the error
    let code: Code
}

// MARK: - FlickrMethod Conformance
extension Flickr.Interestingness: FlickrMethod {
    
    var name: String {
        return "flickr.interestingness.\(self.rawValue)"
    }
}

// MARK: - Argument Parsing
private extension Flickr.Argument {
    
    var name: String {
        switch self {
        case .perPage:
            return "per_page"
        case .page:
            return "page"
        case .extras:
            return "extras"
        }
    }
    
    var value: String {
        switch self {
        case let .perPage(value):
            return String(value)
        case let .page(value):
            return String(value)
        case let .extras(values):
            return values.map({ $0.name }).joined(separator: ",")
        }
    }
    
    var queryItem: URLQueryItem {
        return URLQueryItem(name: name, value: value)
    }
}

// MARK: - Extras parsing
private extension Flickr.Extra {
    
    var name: String {
        switch self {
        case .urlZ:
            return "url_z"
        case .dateTaken:
            return "date_taken"
        default:
            return rawValue
        }
    }
}
