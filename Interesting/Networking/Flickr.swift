//
//  Flickr.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import Foundation

/// A namespace for other eums and values to be used internally in the app
struct Flickr {
    
    private init() { }
    
    /// API Methods based on interestingness
    ///
    /// - getList: Returns the list of interesting photos for the most recent day or a user-specified date.
    enum Interestingness: String {
        case getList
    }
    
    /// Arguments available across the API
    enum Argument {
        case perPage(Int)
        case page(Int)
        case extras([Extra])
    }
    
    /// Extra values that can be requested
    enum Extra: String {
        case tags, geo, urlZ, dateTaken
    }
}
