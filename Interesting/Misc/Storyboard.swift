//
//  Storyboard.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit

/// A protocol used to bring convenience when dequeueing view controllers from within storyboards. The UIViewController extensions provide ease in doing this.
/// Defaults to view controllers within Main.storyboard
protocol Storyboarded: class {
    
    static var storyboardName: String { get }
    
    static var storyboardIdentifier: String { get }
    
    static func fromStoryboard<T: UIViewController>() -> T
}

// MARK: - Base Implementation
extension Storyboarded where Self: UIViewController {
    
    static var storyboardName: String {
        return "Main"
    }
    
    static var storyboardIdentifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    static func fromStoryboard<T: UIViewController>() -> T {
        
        // Get the view controller
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        
        // TODO: Introduce unit tests to check initalization from storyboard or maybe https://github.com/fastred/IBAnalyzer when it matures
        // swiftlint:disable force_cast
        let instance = storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as! T
        
        // Return it
        return instance
    }
}
