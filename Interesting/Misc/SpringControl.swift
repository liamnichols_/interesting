//
//  SpringControl.swift
//  Interesting
//
//  Created by Liam Nichols on 14/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit

/// Simple UIControl subclass that adjusts the scale upon selection with a spring animation
class SpringControl: UIControl {
    
    fileprivate func updateForState(_ animated: Bool) {
        
        // Work out the scale
        let scale: CGFloat
        switch state {
        case [.highlighted], [.selected], [.highlighted, .selected]:
            scale = 0.9
            
        default:
            scale = 1.0
        }
        
        // Create a closure for the updates
        let updateBlock = {
            self.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
        
        // Either animate or just perform the scale change
        if animated {
            UIView.animate(
                withDuration: 0.3,
                delay: 0.0,
                usingSpringWithDamping: 0.6,
                initialSpringVelocity: 0.0,
                options: [],
                animations: updateBlock,
                completion: nil
            )
        } else {
            updateBlock()
        }
    }
    
    override var isSelected: Bool {
        didSet {
            updateForState(true)
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            updateForState(true)
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            updateForState(true)
        }
    }
}
