//
//  DismissSegue.swift
//  Interesting
//
//  Created by Liam Nichols on 14/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit

/// Custom segue designed to dismiss modally presented view controllers
final class DismissSegue: UIStoryboardSegue {
    
    override func perform() {
        source.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}
