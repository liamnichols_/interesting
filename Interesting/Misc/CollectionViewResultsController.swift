//
//  CollectionViewResultsController.swift
//  Interesting
//
//  Created by Liam Nichols on 12/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//
//  Adapted from https://gist.github.com/iwasrobbed/5528897

import UIKit
import CoreData

/// A NSFetchedResultsControllerDelegate designed to handle the updating of a UICollectionView
class CollectionViewResultsController: NSObject {
    
    typealias UpdateBlock = () -> Swift.Void
    
    /// The collection view to update
    weak var collectionView: UICollectionView?
    
    /// Used to help with a reload bug being tracked at http://openradar.appspot.com/12954582
    fileprivate var shouldReload: Bool = false
    
    /// An array of block operations to perform
    fileprivate var updateOperations = [UpdateBlock]()
    
    deinit {
        updateOperations.removeAll(keepingCapacity: false)
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension CollectionViewResultsController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        // Clear any old values ready to start fresh
        shouldReload = false
        updateOperations.removeAll(keepingCapacity: false)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                    atSectionIndex sectionIndex: Int,
                    for type: NSFetchedResultsChangeType) {

        // No point doing updates if the weak reference to collectionView was lost.
        guard collectionView != nil else {
            return
        }
        
        switch type {
        case .insert:
            updateOperations.append({ [weak collectionView = self.collectionView] in
                collectionView?.insertSections(IndexSet(integer: sectionIndex))
            })
            
        case .delete:
            updateOperations.append({ [weak collectionView = self.collectionView] in
                collectionView?.deleteSections(IndexSet(integer: sectionIndex))
            })
        
        case .update:
            updateOperations.append({ [weak collectionView = self.collectionView] in
                collectionView?.reloadSections(IndexSet(integer: sectionIndex))
            })
            
        case .move:
            break // Sections cannot be moved
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {

        // No point doing updates without an indexPath or if the weak reference to collectionView was lost.
        guard let indexPath = indexPath, let collectionView = collectionView else {
            return
        }
        
        switch type {
        case .insert:
            if collectionView.numberOfSections == 0 || collectionView.numberOfItems(inSection: indexPath.section) == 0 {
                shouldReload = true
            } else {
                updateOperations.append({ [weak collectionView = self.collectionView] in
                    collectionView?.insertItems(at: [ indexPath ])
                })
            }
            
        case .delete:
            if collectionView.numberOfItems(inSection: indexPath.section) == 1 {
                shouldReload = true
            } else {
                updateOperations.append({ [weak collectionView = self.collectionView] in
                    collectionView?.deleteItems(at: [ indexPath ])
                })
            }
            
        case .update:
            updateOperations.append({ [weak collectionView = self.collectionView] in
                collectionView?.reloadItems(at: [ indexPath ])
            })
            
        case .move:
            if let newIndexPath = newIndexPath {
                updateOperations.append({ [weak collectionView = self.collectionView] in
                    collectionView?.moveItem(at: indexPath, to: newIndexPath)
                })
            }
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {

        if shouldReload {
            collectionView?.reloadData()
        } else {
            collectionView?.performBatchUpdates({
                for operation in self.updateOperations {
                    operation()
                }
            }, completion: nil)
        }
    }
}
