//
//  Result.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import Foundation

/// Generic enum used for explicitly specifying if an operation succeeded or failed.
enum Result<T> {
    case success(T)
    case failure(Error)
}
