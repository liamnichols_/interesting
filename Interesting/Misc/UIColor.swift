//
//  UIColor.swift
//  Interesting
//
//  Created by Liam Nichols on 14/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit

/// Custom colours used within the app
extension UIColor {
    
    /// The pink color used for liking images
    static var interestingPink: UIColor {
        return UIColor(red: 0.8823529412, green: 0.2705882353, blue: 0.5058823529, alpha: 1.0)
    }
    
    /// The blue color used for dislinking images
    static var interestingBlue: UIColor {
        return UIColor(red: 0.1647058824, green: 0.4117647059, blue: 0.6549019608, alpha: 1.0)
    }
}
