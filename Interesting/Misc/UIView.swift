//
//  UIView.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit

extension UIView {
    
    /// UIStackView doesn't like it if you try to hide the view twice then it won't show again. This is a helper to ensure that isHidden isn't called when it doesn't need to be
    var isHiddenInStack: Bool {
        set {
            if isHidden != newValue {
                isHidden = newValue
            }
        }
        get {
            return isHidden
        }
    }
}
