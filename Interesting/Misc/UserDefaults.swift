//
//  UserDefaults.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import Foundation

/// Custom user default properties used in the app
extension UserDefaults {
    
    /// Determines if the app has shown the welcome content or not. Setting to true will bring the application into the BrowseViewController directly where false will show the WelcomeViewController before moving into the main content
    var hasShownWelcome: Bool {
        set {
            set(newValue, forKey: "INT_HasShownWelcome")
        }
        get {
            return bool(forKey: "INT_HasShownWelcome")
        }
    }
}
