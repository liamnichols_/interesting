//
//  PhotosController.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import Foundation
import CoreData
import SDWebImage

extension Notification.Name {
    
    /// A notification sent from PhotosController when its state changes. This means that the isUpdating value has changed meaning that an update could have started or stopped. Object contains the instance of PhotosController that triggered the notification.
    static let photosControllerStateDidChange = Notification.Name("PhotosController.stateDidChange")
}

/// The controller designed to obtain interesting photos from a remote service and parse them into core data for use within the app.
class PhotosController {
    
    /// The controller used for fetching photo data from a remote server
    private let flickrController = FlickrController()
    
    /// The error from the last update (if any). Cleared upon a new update starting
    private(set) var updateError: Error?
    
    /// Determines if the controller is currently updating or not
    private(set) var isUpdating: Bool = false {
        didSet {
            
            // Post a notification if this gets set back to false
            if oldValue != isUpdating {
                NotificationCenter.default.post(name: .photosControllerStateDidChange, object: self)
            }
        }
    }
    
    /// If one is not already in progress, this method will trigger an update of the interesting photos and load them into core data.
    func updateInterestingPhotosIfNeeded() {
    
        // Could do with cleaning this up a little but the NotificationCenter pattern allows for us to easily trigger updates early on in the app lifecycle but also show progress within the BrowseViewController so leaving it as it is for now.
        
        // Make sure we aren't already updating
        guard isUpdating == false else {
            return
        }
        
        // Block other calls to this method
        isUpdating = true
        updateError = nil
        
        // Get the arguments for the request
        let arguments: [Flickr.Argument] = [
            .perPage(100), // I've set this to 100 to experience the no photos remaining state but it can go to 500
            .extras([
                .tags,
                .geo,
                .urlZ,
                .dateTaken
            ])
        ]
        
        // Fetch a copy of the latest interesting images from Flickr
        flickrController.get(from: Flickr.Interestingness.getList, with: arguments) { result in
            
            // Check the result of the API call
            let response: FlickrResponse
            switch result {
            case let .failure(error):
                self.updateError = error
                self.isUpdating = false
                return
            
            case let .success(res):
                response = res
            }
            
            // Attempt to read Photos from the response
            let photos: FlickrPhotos
            do {
                photos = try response.getPhotos()
            } catch {
                self.updateError = error
                self.isUpdating = false
                return
            }
            
            // Now parse the photos in core data
            PersistentContainer.shared.performBackgroundTask { context in
                
                // Wrap in a try block to catch any core data errors
                do {

                    // Perform a batch update to mark all the images in the database as not browsable
                    let request = NSBatchUpdateRequest(entity: Photo.entity())
                    request.propertiesToUpdate = [ "isBrowsable": false, "sortIndex": 0 ]
                    try context.execute(request)
                    
                    // Loop through all the new photos objects and update or create them 
                    for (idx, flickrPhoto) in photos.photo.enumerated() {
                        
                        // Update all the parameters on the Photo
                        let photo = Photo.get(with: flickrPhoto, in: context)
                        photo.isBrowsable = true
                        photo.sortIndex = Int64(idx)
                        
                        // Add the first 5 image urls to be prefetched while parsing
                        if let url = photo.imageURL, idx <= 5 {
                            SDWebImagePrefetcher.shared().prefetchURLs([ url ])
                        }
                    }
                    
                    // Save
                    try context.save()
                    
                    // Complete
                    self.updateError = nil
                    self.isUpdating = false
                    
                } catch {
                    
                    // Fail
                    self.updateError = error
                    self.isUpdating = false
                }
            }
        }
    }
}
