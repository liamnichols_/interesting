//
//  ExploreCollectionCell.swift
//  Interesting
//
//  Created by Liam Nichols on 14/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit
import SDWebImage

final class ExploreCollectionCell: UICollectionViewCell {
    
    /// The image view used to show the photo
    @IBOutlet fileprivate var imageView: UIImageView!
    
    /// The identifier used to identify this cell type (both reuse identifier and nib name)
    static let identifier = "ExploreCollectionCell"
    
    /// Registers the BrowseCollectionCell and its nib in the specified collectionView
    ///
    /// - Parameter collectionView: The collection view to register the cell within
    static func register(in collectionView: UICollectionView) {
        collectionView.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photo = nil // clear the photo
    }
    
    var photo: Photo? {
        didSet {
            
            // Cancel any previous image loads and load the this image if we have it's url
            imageView.sd_cancelCurrentImageLoad()
            imageView.sd_setImage(with: photo?.imageURL)
        }
    }

    // UIMenu action for removing this photo
    @objc func remove(_ controller: UIMenuController) {
    
        // Get the decision to reverse
        guard let decisionId = photo?.decision?.objectID else {
            return
        }
        
        // Create a background context to make the change
        PersistentContainer.shared.performBackgroundTask { context in
            
            do {
                
                // Get the decision on this context
                guard let decision = context.object(with: decisionId) as? Decision else {
                    return
                }
                
                // Update it and save
                decision.isInterested = false
                try context.save()
                
            } catch {
                print("Error reversing decision:", error)
            }
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            
            // Work out the scale
            let scale: CGFloat = isHighlighted ? 0.9 : 1.0
            
            // A closure for the transform update
            let updateBlock = {
                self.contentView.transform = CGAffineTransform(scaleX: scale, y: scale)
            }
            
            // Animate the update
            UIView.animate(
                withDuration: 0.3,
                delay: 0.0,
                usingSpringWithDamping: 0.6,
                initialSpringVelocity: 0.0,
                options: [],
                animations: updateBlock,
                completion: nil
            )
        }
    }
}
