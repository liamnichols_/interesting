//
//  BrowseCollectionCell.swift
//  Interesting
//
//  Created by Liam Nichols on 12/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit
import SDWebImage

final class BrowseCollectionCell: UICollectionViewCell {
    
    /// The identifier used to identify this cell type (both reuse identifier and nib name)
    static let identifier = "BrowseCollectionCell"
    
    /// Registers the BrowseCollectionCell and its nib in the specified collectionView
    ///
    /// - Parameter collectionView: The collection view to register the cell within
    static func register(in collectionView: UICollectionView) {
        collectionView.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
    }
    
    /// Just a helper for fetching the web image manager
    fileprivate var imageManager: SDWebImageManager {
        return SDWebImageManager.shared()
    }
    
    /// The operation for downloading the image on this cell
    fileprivate var imageOperation: SDWebImageOperation?
    
    /// The button used to retry the image download if it previously failed
    @IBOutlet fileprivate var retryButton: UIButton!
    
    /// The activity indicator used when loading the image
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    
    /// The container view that the image is allowed to fill
    @IBOutlet fileprivate var containerView: UIView!
    
    /// The wrapper view used for the image shaddow
    @IBOutlet fileprivate var imageWrapper: UIView!
    
    /// The image view used to display the content
    @IBOutlet fileprivate var imageView: UIImageView!
    
    /// The image view used to show the decision chosen when transitioning out
    @IBOutlet fileprivate var decisionImageView: UIImageView!
    
    // Constraints for the content view
    @IBOutlet fileprivate var topConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var leftConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var bottomConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var rightConstraint: NSLayoutConstraint!
    
    /// Insets used to adjust the positioning of inner content (to allow for layout guides etc)
    var contentInset: UIEdgeInsets = .init(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0) {
        didSet {
            if oldValue != contentInset {
                topConstraint.constant = contentInset.top
                leftConstraint.constant = contentInset.left
                bottomConstraint.constant = contentInset.bottom
                rightConstraint.constant = contentInset.right
            }
        }
    }
    
    /// The photo to be displayed in this cell
    var photo: Photo? {
        didSet {
            reloadContent()
        }
    }
    
    private func reloadContent() {
        
        // Cancel an image operation if there is one
        imageOperation?.cancel()
        imageOperation = nil
        
        // If the photo changes, we need to reposition the image view
        repositionImageView(using: CGFloat(photo?.aspectRatio ?? 1.0))
        
        // If we have an image to load
        if let imageURL = photo?.imageURL {
            
            // Either pull the image out the cache or download it
            if let image = imageManager.imageCache?.imageFromCache(forKey: imageManager.cacheKey(for: imageURL)) {
                updateImageView(with: image)
            } else {
                updateImageView(with: nil, isLoading: true)
                imageOperation = imageManager.loadImage(with: imageURL, options: [ ], progress: nil) { [weak self] image, _, _, _ , _, _ in
                    self?.updateImageView(with: image, showRetryIfNeeded: true)
                }
            }
        } else {
            updateImageView(with: nil)
        }
    }
    
    private func updateImageView(with image: UIImage?, isLoading: Bool = false, showRetryIfNeeded: Bool = false) {
        
        // Update the aspect ratio based on what we have and not what Photo thinks we have
        if let size = image?.size {
            repositionImageView(using: size.width / size.height)
        }
        
        // Set the image
        imageView.image = image
        
        // Start or stop the activity indicator
        if isLoading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
        
        // Set the appropriate background color
        imageView.backgroundColor = image == nil ? .white : .clear
        
        // Show the retry button if the image is not present and we were attempting to load it previously
        retryButton.isHidden = !(image == nil && showRetryIfNeeded)
    }
    
    override func prepareForReuse() {
        
        // Make sure the photo gets cleared out
        photo = nil
        
        // Reset the transition state
        isTransitioningOut(with: 0.0, isInterested: false)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Apply some final style to the image view
        imageView.backgroundColor = .white
        imageView.layer.cornerRadius = 10.0
        imageView.layer.masksToBounds = true
        imageWrapper.layer.shadowColor = UIColor(white: 1.0, alpha: 0.25).cgColor
        imageWrapper.layer.shadowOpacity = 1.0
        imageWrapper.layer.shadowRadius = 10.0
        
        // Give the decisionImageView some constraints as we're playing with it's transform
        decisionImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            decisionImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            decisionImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
        ])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // If the photo changes, we need to reposition the image view
        containerView.layoutIfNeeded() // Always ensure that the container view has been sized correctly before attempting to reposition
        repositionImageView(using: CGFloat(photo?.aspectRatio ?? 1.0))
    }
    
    private func repositionImageView(using aspectRatio: CGFloat) {
        
        // Ensure that the image view is centered
        imageWrapper.center = CGPoint(x: containerView.bounds.midX, y: containerView.bounds.midY)
        
        // Get maximum bounds that we're allowed
        let containerBounds = containerView.bounds
        
        // Detect if the image requires more heigh than we have available, if so we need to work with the vertical axis
        if containerBounds.width / aspectRatio > containerBounds.height {
            imageWrapper.bounds = CGRect(origin: .zero, size: CGSize(
                width: CGFloat(ceil(containerBounds.height * aspectRatio)),
                height: containerBounds.height)
            )
        } else {
            imageWrapper.bounds = CGRect(origin: .zero, size: CGSize(
                width: containerBounds.width,
                height: CGFloat(ceil(containerBounds.width / aspectRatio)))
            )
        }
    }
    
    @IBAction private func retryButtonPressed(_ sender: UIButton) {
        
        // Just trigger a reload of the content as the image is all we have to show
        reloadContent()
    }
    
    func isTransitioningOut(with percentage: CGFloat, isInterested: Bool) {
        
        // Slowly dim the view out
        imageView.alpha = 1.0 - min(percentage * 2.0, 1.0)
        
        // Set the correct decision image
        decisionImageView.tintColor = isInterested ? .interestingPink : .interestingBlue
        decisionImageView.image = isInterested ? #imageLiteral(resourceName: "Love_Large") : #imageLiteral(resourceName: "Hate_Large")
        
        // Fade the image in fairly quickly
        decisionImageView.alpha = min(percentage * 8.0, 1.0)
        
        // Work out the scale and also apply that
        let scale = max(0.0001, min(percentage * 8.0, 1.0))
        decisionImageView.transform = CGAffineTransform(scaleX: scale, y: scale)
    }
    
    /// Returns true if the supplied gesture recognizer is interacting with content of the cell (the image view)
    ///
    /// - Parameter gesture: The gesture to test
    /// - Returns: The result of the test
    func didGestureInteractWithContent(gesture: UIGestureRecognizer) -> Bool {
        return imageView.frame.contains(gesture.location(in: imageView))
    }
}
