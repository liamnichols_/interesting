//
//  NoPhotosCollectionCell.swift
//  Interesting
//
//  Created by Liam Nichols on 14/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit

final class NoPhotosCollectionCell: UICollectionViewCell {
    
    /// The identifier used to identify this cell type (both reuse identifier and nib name)
    static let identifier = "NoPhotosCollectionCell"
    
    /// Registers the BrowseCollectionCell and its nib in the specified collectionView
    ///
    /// - Parameter collectionView: The collection view to register the cell within
    static func register(in collectionView: UICollectionView) {
        collectionView.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
    }
}
