//
//  NavigationController.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit

final class NavigationController: UINavigationController {
    
    /// The photos controller
    let photosController = PhotosController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Trigger an update of interesting photos
        photosController.updateInterestingPhotosIfNeeded()
        
        // Show the correct content
        if UserDefaults.standard.hasShownWelcome {
            setContentToBrowser()
        } else {
            setContentToWelcome()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

// MARK: - Transitions
extension NavigationController {
    
    func setContentToWelcome() {
        viewControllers = [ WelcomeViewController.fromStoryboard() ]
    }
    
    func setContentToBrowser() {
        
        // Flag that we've shown the welcome screen
        UserDefaults.standard.hasShownWelcome = true
        
        // Replace the view controllers with the BrowseViewController
        viewControllers = [ BrowseViewController.fromStoryboard() ]
    }
}

// MARK: - Helpers
extension UIViewController {
    
    /// Helper for accessing the NavigationController if it's there
    var interestingNavigationController: NavigationController? {
        return navigationController as? NavigationController
    }
}
