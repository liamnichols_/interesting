//
//  PhotoViewController.swift
//  Interesting
//
//  Created by Liam Nichols on 14/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit
import SDWebImage

final class PhotoViewController: UIViewController, Storyboarded {
    
    /// The photo to represent in this view controller
    var photo: Photo?
    
    @IBOutlet fileprivate var imageView: UIImageView!
    @IBOutlet fileprivate var scrollView: UIScrollView!
    @IBOutlet fileprivate var topConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var leadingConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var bottomConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var trailingConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the prompt to the photo title (or clear it)
        if let title = photo?.title, !title.isEmpty {
            navigationItem.prompt = title
        } else {
            navigationItem.prompt = nil
        }
        
        // See if the image was already cached
        let cacheKey = SDWebImageManager.shared().cacheKey(for: photo?.imageURL)
        if let image = SDImageCache.shared().imageFromCache(forKey: cacheKey) {
            
            // Use this image and update the view
            imageView.image = image
            
            // Make sure the image view is centered properly
            positionImageView()
         
            // Add the gesture recognizer to toggle scroll bars like the photos app
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapGestureRecognized(_:))))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Check on the image state
        checkImageState()
    }
    
    /// Determines if all the focus should be on the image (full screen mode)
    var focusOnImage: Bool = false {
        didSet {
            if focusOnImage != oldValue {
                updateImageFocus()
            }
        }
    }
}

// MARK: - Actions
private extension PhotoViewController {
    
    @objc func tapGestureRecognized(_ gesture: UITapGestureRecognizer) {
        
        // Toggle the focus on image bool
        focusOnImage = !focusOnImage
    }
    
    @IBAction func shareButtonPressed(_ sender: UIBarButtonItem) {
        
        // Make sure the image is present
        guard let image = imageView.image else {
            return
        }
        
        // Present the activity view controller
        let viewController = UIActivityViewController(activityItems: [ image ], applicationActivities: nil)
        present(viewController, animated: true, completion: nil)
    }
}

// MARK: - Image State
private extension PhotoViewController {
    
    // Note: If there was more time, i'd load a full high resolution image here but for now we're just going to use whats in the cache or present this error
    
    /// Checks if the image is present, if it isn't then an error is shown
    func checkImageState() {
        
        // If the image view's image was nil, show an alert and pop back
        guard imageView.image == nil else {
            return
        }
        
        // Create an alert controller to show an error
        let alertController = UIAlertController(
            title: NSLocalizedString("Error", comment: ""),
            message: NSLocalizedString("There was an error loading the image.", comment: ""),
            preferredStyle: .alert
        )
        
        // Add a dismiss action that pops back to the root view controller
        alertController.addAction(UIAlertAction(
            title: NSLocalizedString("Dismiss", comment: ""),
            style: .cancel,
            handler: { _ in
                
                // Pop back to the root view controller
                _ = self.navigationController?.popViewController(animated: true)
        }))
        
        // Present the error
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Focus & Rotating
extension PhotoViewController {

    fileprivate func updateImageFocus() {
        
        // Update the status bar
        setNeedsStatusBarAppearanceUpdate()
        
        // Show or hide the nav bar
        navigationController?.setNavigationBarHidden(focusOnImage, animated: true)
        
        // Change the background color
        UIView.animate(withDuration: 0.2) {
            self.view.backgroundColor = self.focusOnImage ? .black : .white
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return focusOnImage // we don't want a status bar if we're focusing on the image
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Reposition the image view
        coordinator.animate(alongsideTransition: { _ in
            self.positionImageView()
        }, completion: nil)
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
}

// MARK: - Scroll Position
private extension PhotoViewController {
    
    func positionImageView() {
    
        // Get the relevant sizes
        let imageSize = imageView.image?.size ?? .init(width: 100.0, height: 100.0)
        let scrollViewSize = scrollView.bounds.size
        
        // Work out the minimum zoom scale
        let outZoom = min(min(scrollViewSize.width / imageSize.width, scrollViewSize.height / imageSize.height), 1.0)
        scrollView.minimumZoomScale = outZoom
        scrollView.maximumZoomScale = outZoom * 4.0 // Can zoom in 4x
        
        // Update the constraints
        updateImageViewConstraints()
        
        // Zoom out so the whole image is visible
        scrollView.zoomScale = outZoom
    }
    
    func updateImageViewConstraints() {
     
        // Get the relevant sizes
        let imageSize = imageView.image?.size ?? .init(width: 100.0, height: 100.0)
        let scrollViewSize = scrollView.bounds.size
        
        // Work out how much padding we need (if any)
        let hPadding = max(0.0, (scrollViewSize.width - scrollView.zoomScale * imageSize.width) / 2)
        let vPadding = max(0.0, (scrollViewSize.height - scrollView.zoomScale * imageSize.height) / 2)
        
        // Update the constraints with the padding values
        leadingConstraint.constant = hPadding
        trailingConstraint.constant = hPadding
        topConstraint.constant = vPadding
        bottomConstraint.constant = vPadding
        
        // Layout subviews if we need to
        view.layoutIfNeeded()
    }
}

// MARK: - UIScrollViewDelegate
extension PhotoViewController: UIScrollViewDelegate {
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        updateImageViewConstraints()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
