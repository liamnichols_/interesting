//
//  BrowseViewController.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage

final class BrowseViewController: UIViewController, Storyboarded {
    
    @IBOutlet fileprivate var collectionView: UICollectionView!
    @IBOutlet fileprivate var placeholderStackView: UIStackView!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var placeholderLabel: UILabel!
    @IBOutlet fileprivate var placeholderButton: UIButton!
    @IBOutlet fileprivate var actionContentView: UIVisualEffectView!
    @IBOutlet fileprivate var actionStackView: UIStackView!
    @IBOutlet fileprivate var likeButton: UIControl!
    @IBOutlet fileprivate var dislikeButton: UIControl!
    
    /// The photos to be displayed
    fileprivate var photos: [Photo] = []
    
    /// The photos that the user has marked as interesting
    fileprivate var photosOfInterest: [Photo] = []
    
    /// Used to track loading of content and animations on view appearance
    fileprivate var hasLoaded: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup our collection view
        BrowseCollectionCell.register(in: collectionView)
        NoPhotosCollectionCell.register(in: collectionView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // If we haven't loaded anything yet then load it
        if !hasLoaded {
            loadInitialState()
            hasLoaded = true
        }
    }
    
    /// Manually process the visible photo
    ///
    /// - Parameters:
    ///   - isInterested: If the user is interested in the photo
    fileprivate func manuallyProcessVisiblePhoto(isInterested: Bool) {
        
        // Make sure there are actually photos to process
        guard photos.count > 0 else {
            return
        }
        
        // If we have an index path and isInterested == true, mark this as a photo of interest
        if let indexPath = collectionView.indexPathForItem(at: CGPoint(x: 0.0, y: collectionView.bounds.midY)), isInterested {
            photosOfInterest.append(photos[indexPath.row])
        }
        
        // Scroll to the next photo
        collectionView.setContentOffset(CGPoint(x: 0.0, y: collectionView.bounds.height), animated: true)
    }
    
    /// Processes any photos that the user has interacted with
    fileprivate func processPastPhotos() {
        
        // Make sure there is an index path (there will be)
        guard let indexPath = collectionView.indexPathForItem(at: CGPoint(x: 0.0, y: collectionView.bounds.midY)),
            photos.count >= indexPath.row,
            indexPath.row > 0 else {
            return
        }
        
        // Get the items up until the visible row
        let results = photos[0..<indexPath.row]
        
        // Remove them from the data source
        photos.removeFirst(indexPath.row)
        
        // Reload the datasource and scroll back to the top so the user can't tell
        collectionView.reloadData()
        collectionView.contentOffset = .zero
        
        // Perform some updates as a background task
        PersistentContainer.shared.performBackgroundTask { context in
            
            // Mark them as browsed and also add an interest if the user liked it
            for result in results {
                
                // Get the photo in this context
                guard let photo = context.object(with: result.objectID) as? Photo else {
                    continue
                }
                
                // Create a decision object and attach it
                let decision = Decision(context: context)
                decision.isInterested = self.photosOfInterest.contains(result)
                decision.date = Date()
                photo.decision = decision
            }
            
            // Save the changes
            // Note: It's rare that anything would prevent a save from working (i.e no disk space), for this demo there is no need to handle a faulure here as it's so unlikely.
            try? context.save()
        }
    }
}

// MARK: - Actions
private extension BrowseViewController {
    
    @IBAction func placeholderButtonPressed(_ sender: UIControl) {
        
        // If we tapped the placeholder while there was an update error, have another go at updating
        if interestingNavigationController?.photosController.updateError != nil {
            interestingNavigationController?.photosController.updateInterestingPhotosIfNeeded()
        }
    }
    
    @IBAction func likeButtonPressed(_ sender: UIControl) {
        manuallyProcessVisiblePhoto(isInterested: true)
    }
    
    @IBAction func dislikeButtonPressed(_ sender: UIControl) {
        manuallyProcessVisiblePhoto(isInterested: false)
    }
    
    @IBAction func doubleTapGestureRecognized(_ sender: UITapGestureRecognizer) {
        
        // Work out hte index path for this tap and get its cell to query
        guard let indexPath = collectionView.indexPathForItem(at: sender.location(in: collectionView)),
            let cell = collectionView.cellForItem(at: indexPath) as? BrowseCollectionCell else {
                return
        }
        
        // If the double tap was within the photo then process it as a like
        if cell.didGestureInteractWithContent(gesture: sender) {
            manuallyProcessVisiblePhoto(isInterested: true)
        }
    }
}

// MARK: - UIScrollViewDelegate
extension BrowseViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        // Process the photos as we've stopped scrolling
        processPastPhotos()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // If we aren't decelerating then action now, otherwise wait for scrollViewDidEndDecelerating:
        if !decelerate {
            processPastPhotos()
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
        // Process the past photos now the animation has finished
        processPastPhotos()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // Get the first cell as we want to manipulate it as the user scrolls it off the screen
        guard photos.count > 0, let cell = collectionView.cellForItem(at: IndexPath(row: 0, section: 0)) as? BrowseCollectionCell,
            let photo = cell.photo else {
                return
        }
        
        // Transition the cell out
        let percentage = min(max(collectionView.contentOffset.y / collectionView.bounds.height, 0.0), 1.0)
        cell.isTransitioningOut(with: percentage, isInterested: photosOfInterest.contains(photo))
    }
}

// MARK: - UICollectionViewDataSource
extension BrowseViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count + 1 // additional cell for the "no photos" cell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // We have a "no photos" row
        if indexPath.row >= photos.count {
            return collectionView.dequeueReusableCell(withReuseIdentifier: NoPhotosCollectionCell.identifier, for: indexPath)
        }
        
        // Get the cell and the photo to show
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BrowseCollectionCell.identifier, for: indexPath)
        
        // Apply some settings to the BrowseCollectionCell properties
        if let cell = cell as? BrowseCollectionCell {
            
            cell.photo = photos[indexPath.row]
            cell.contentInset = UIEdgeInsets(
                top: 20.0 + topLayoutGuide.length,
                left: 20.0,
                bottom: 20.0 + bottomLayoutGuide.length + actionContentView.bounds.height,
                right: 20.0
            )
        }
        
        // Return the cell
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension BrowseViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // The cell is always the same size of the collection view so that things page properly
        return collectionView.bounds.size
    }
}

extension BrowseViewController: UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
        // Prefetch any images where possible
        SDWebImagePrefetcher.shared().prefetchURLs(
            indexPaths.flatMap({
                photos[safe: $0.row]?.imageURL
            })
        )
    }
    
    func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
        
        // We'll probably need the images later so lets not worry about this
    }
}

// MARK: - State
private extension BrowseViewController {
    
    @objc func photosControllerStateDidChangeNotification(_ notification: Notification) {
        
        // Get the photos view controller
        if let photosController = notification.object as? PhotosController {
            
            // Ensure we move back to the main queue to update the state
            DispatchQueue.main.async {
                self.updateState(from: photosController)
            }
        }
    }
    
    func loadInitialState() {
        
        // Add a gesture recognizer to detect double taps
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.doubleTapGestureRecognized(_:)))
        recognizer.numberOfTapsRequired = 2
        collectionView.addGestureRecognizer(recognizer)
        
        // Make sure the like/dislike buttons are tinted
        likeButton.tintColor = .interestingPink
        dislikeButton.tintColor = .interestingBlue
        
        // If we have the PhotosController, add a notification to listen for updates
        if let photosController = interestingNavigationController?.photosController {
            
            // Update the state
            updateState(from: photosController)
            
            // Listen for future changes
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(photosControllerStateDidChangeNotification(_:)),
                name: .photosControllerStateDidChange,
                object: photosController
            )
        }
    }
    
    func updateState(from photosController: PhotosController) {
        
        // Note: Ideally we would really hanlde error states differently rather than just showing a basic placeholder label. I've kept this quite simple for now but if we were to do anything more complex with the differnet UI states then this would be the first thing to be refactored. Differnet child view controllers would probably be a good approach.
        
        // Work out the current state
        if photosController.updateError != nil {
            
            // TODO: Make use of the `updateError` to be more informative to the user
            
            // Show an error
            activityIndicator.stopAnimating()
            placeholderLabel.text = NSLocalizedString("There was an error loading the images from Flickr. Please try again later.", comment: "")
            placeholderLabel.isHiddenInStack = false
            placeholderButton.setTitle(NSLocalizedString("Try Again", comment: ""), for: .normal)
            placeholderButton.isHiddenInStack = false
            collectionView.isHidden = true
            actionStackView.isHidden = true
            
        } else if photosController.isUpdating {
            
            // Show an activity indicator
            activityIndicator.startAnimating()
            placeholderLabel.isHiddenInStack = true
            placeholderButton.isHiddenInStack = true
            collectionView.isHidden = true
            actionStackView.isHidden = true
            
        } else {
            
            // Build a fetch for the photos that are browsable and had not yet had a decision made
            let request: NSFetchRequest<Photo> = Photo.fetchRequest()
            request.sortDescriptors = [ .init(key: "sortIndex", ascending: true) ]
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [
                NSPredicate(format: "isBrowsable == true"),
                NSPredicate(format: "decision == nil")
            ])
            
            // Fetch them all
            if let photos = try? PersistentContainer.shared.viewContext.fetch(request) {
                self.photos = photos
            }
            
            // Reload the collection view
            collectionView.reloadData()
            
            // Show the collection view
            activityIndicator.stopAnimating()
            placeholderLabel.isHiddenInStack = true
            placeholderButton.isHiddenInStack = true
            showPhotoContent(animated: true)
        }
    }
}

// MARK: - Animations
private extension BrowseViewController {
    
    func showPhotoContent(animated: Bool) {
        
        // Set the initial states if we're using animations
        if animated {
            likeButton.alpha = 0.0
            dislikeButton.alpha = 0.0
            collectionView.alpha = 0.0
            actionStackView.transform = CGAffineTransform(translationX: 0.0, y: actionStackView.bounds.height)
        }
        
        // Unhide the views
        actionStackView.isHidden = false
        collectionView.isHidden = false
        
        // Animate to the shown state if we want animations
        if animated {
            UIView.animate(withDuration: 0.4) {
                self.collectionView.alpha = 1.0
            }
            UIView.animate(withDuration: 0.4, delay: 0.25, options: [ ], animations: { 
                self.likeButton.alpha = 1.0
                self.dislikeButton.alpha = 1.0
                self.actionStackView.transform = .identity
            }, completion: nil)
        }
    }
}
