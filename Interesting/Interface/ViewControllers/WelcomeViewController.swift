//
//  WelcomeViewController.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit

final class WelcomeViewController: UIViewController, Storyboarded {
    
    @IBOutlet fileprivate var backgroundImageView: UIImageView!
    @IBOutlet fileprivate var stackView: UIStackView!
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var hintLabels: [UILabel]!
    @IBOutlet fileprivate var actionButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // A paragraph style to use on each of our hint labels
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.firstLineHeadIndent = 0.0
        paragraphStyle.headIndent = 12.0
        
        // Apply a paragraph style to each hint label
        for label in hintLabels {
            label.attributedText = NSMutableAttributedString(
                string: label.text ?? "",
                attributes: [
                    NSParagraphStyleAttributeName: paragraphStyle
                ]
            )
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Animate the content into
        animateContentIn()
    }
}

// MARK: - Animations
private extension WelcomeViewController {
    
    func animateContentIn() {
        
        // Configure everything to the starting state
        titleLabel.alpha = 0.0
        actionButton.alpha = 0.0
        for label in hintLabels {
            label.alpha = 0.0
            label.transform = CGAffineTransform(translationX: 200.0, y: 0.0)
        }
        
        // A delay to stagger each entrance
        var delay: TimeInterval = 1.0
        
        // Animate the title label to start with
        UIView.animate(withDuration: 0.8, delay: delay, options: [ .curveEaseInOut ], animations: {
            self.titleLabel.alpha = 1.0
        }, completion: nil)
        
        // Increment the delay
        delay += 0.8
        
        for label in hintLabels {
            
            // Add an animation for the labels entrance
            UIView.animate(withDuration: 1.0, delay: delay, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: [], animations: {
                label.alpha = 1.0
                label.transform = .identity
            }, completion: nil)
            
            // Increase the delay for each label
            delay += 0.075
        }
        
        // Increment the delay again 
        delay += 1.0
        
        // Animate the action button in at the end
        UIView.animate(withDuration: 0.8, delay: delay, options: [ .curveEaseInOut ], animations: {
            self.actionButton.alpha = 1.0
        }, completion: nil)
    }
}

// MARK: - Actions
private extension WelcomeViewController {
    
    @IBAction func actionButtonPressed(_ sender: UIButton) {
        
        // Animate all content off the screen
        UIView.animate(withDuration: 0.4, delay: 0.1, options: [ .curveEaseIn ], animations: {
            
            // Translate some of the content off the screen
            self.stackView.transform = CGAffineTransform(translationX: 0.0, y: -self.stackView.frame.maxY)
            self.backgroundImageView.transform = CGAffineTransform(translationX: 0.0, y: self.backgroundImageView.frame.maxY)
            
        }, completion: { _ in
            
            // Delay this briefly as the BrowseViewController is designed to kick things off right away
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.interestingNavigationController?.setContentToBrowser()
            }
        })
    }
}
