//
//  ExploreViewController.swift
//  Interesting
//
//  Created by Liam Nichols on 14/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit
import CoreData

/// The number of columns in the collection view
private let itemColumnCount: Int = 3

/// The internal spacing amount between items
private let itemSpacing: CGFloat = 10.0

final class ExploreNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return topViewController?.preferredInterfaceOrientationForPresentation ?? .portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return topViewController?.supportedInterfaceOrientations ?? .portrait
    }
}

final class ExploreViewController: UIViewController, Storyboarded {
    
    @IBOutlet fileprivate var collectionView: UICollectionView!
    @IBOutlet fileprivate var shadowView: UIView!
    
    /// The data source of the collection view
    fileprivate var fetchedResultsController: NSFetchedResultsController<Decision>?
    
    /// The delegate for the fetched results controller
    fileprivate var resultsController = CollectionViewResultsController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register custom menu actions
        setupForMenuController()
        
        // Register cell nib
        ExploreCollectionCell.register(in: collectionView)
        
        // Setup the fetched results controller
        setupFetchedResultsController()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Set the collection view insets as we have custom headers to avoid
        collectionView.contentInset = UIEdgeInsetsMake(shadowView.frame.maxY, 0.0, bottomLayoutGuide.length, 0.0)
        collectionView.scrollIndicatorInsets = collectionView.contentInset
    }
}

// MARK: - UIMenuController
extension ExploreViewController {
    
    func setupForMenuController() {
    
        // Add the custom action, note that the selector is for a remove: function on the *cell*
        UIMenuController.shared.menuItems = [
            .init(title: "Remove", action: #selector(ExploreCollectionCell.remove(_:)))
        ]
    }
    
    // For the UIMenuController
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    // The exact same method signature has to also exist here although it's never called otherwise UIKit will crash. Some sort of Swift bug. This method will never be called though, just the one on ExploreCollectionCell
    @objc func remove(_ controller: UIMenuController) {
        
    }
}

// MARK: - Data Source
private extension ExploreViewController {
    
    func setupFetchedResultsController() {
    
        // Create the the fetch request
        let context = PersistentContainer.shared.viewContext
        let request: NSFetchRequest<Decision> = Decision.fetchRequest()
        request.sortDescriptors = [ NSSortDescriptor(key: "dateValue", ascending: false) ]
        request.predicate = NSPredicate(format: "isInterested == true")
        
        // Initalize the fetched results controller
        fetchedResultsController = NSFetchedResultsController(
            fetchRequest: request,
            managedObjectContext: context,
            sectionNameKeyPath: nil, 
            cacheName: nil
        )
        
        // Make sure something is there to update from the fetched results controller
        resultsController.collectionView = collectionView
        fetchedResultsController?.delegate = resultsController
        
        // Perform the initial fetch
        do {
            try fetchedResultsController?.performFetch()
        } catch {
            print("Error performing initial fetch for ExploreViewController content:", error)
        }
    }
}

// MARK: - UICollectionViewDataSource
extension ExploreViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return fetchedResultsController?.sections?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedResultsController?.sections?[section].numberOfObjects ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Dequeue the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExploreCollectionCell.identifier, for: indexPath)
        
        // Get the photo and update the cell
        (cell as? ExploreCollectionCell)?.photo = fetchedResultsController?.object(at: indexPath).photo
        
        // Return it
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension ExploreViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        // View this photo
        let viewController: PhotoViewController = .fromStoryboard()
        viewController.photo = fetchedResultsController?.object(at: indexPath).photo
        show(viewController, sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        
        // We want to show the menu on long press
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        
        // We want a single menu item to "remove" the image from the explore screen
        return action == #selector(ExploreCollectionCell.remove(_:))
    }
    
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        
        // This method is never called for custom UIMenuItem's but you have to implement it anyway
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ExploreViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        // Work out how to fit the cells in the layout
        let totalSpacingAmount = itemColumnCount > 1 ? itemSpacing * CGFloat(itemColumnCount - 1) : 0.0
        let totalItemSpace = collectionView.bounds.width - totalSpacingAmount
        let sizePerItem = totalItemSpace / CGFloat(itemColumnCount)
        return CGSize(width: sizePerItem, height: sizePerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
}
