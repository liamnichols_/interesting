//
//  CoreData.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import CoreData

protocol CoreData: class {
    
    var viewContext: NSManagedObjectContext { get }
    
    func loadPersistentStores(completionHandler block: @escaping (NSPersistentStoreDescription, Error?) -> Swift.Void)
    
    func newBackgroundContext() -> NSManagedObjectContext
    
    func performBackgroundTask(_ block: @escaping (NSManagedObjectContext) -> Swift.Void)
}

class PersistentContainer: NSPersistentContainer, CoreData {
    
    static let shared: CoreData = PersistentContainer()
    
    private convenience init() {
        self.init(name: "Interesting")
        
        // Load the persistent stores
        self.loadPersistentStores { _, error in
            print("[PersistentContainer] Loaded. Error: \(error)")
        }
        
        // Allow for background changes being merged back
        self.viewContext.automaticallyMergesChangesFromParent = true
    }
}
