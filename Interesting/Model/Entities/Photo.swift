//
//  Photo.swift
//  Interesting
//
//  Created by Liam Nichols on 11/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import UIKit
import CoreData

extension Photo {
    
    /// Finds or creates a Photo with the given flicker photo identifier in the specified context and then updates all the flickr related properties on the photo object
    ///
    /// - Parameters:
    ///   - flickrPhoto: The FlickrPhoto representation of the Photo to find
    ///   - context: The context to search or update
    /// - Returns: The newly created photo or the first result with the specified identifier with the properties updated
    static func get(with flickrPhoto: FlickrPhoto, in context: NSManagedObjectContext) -> Photo {
        
        let photo = get(with: flickrPhoto.identifier, in: context)
        photo.ownerId = flickrPhoto.owner
        photo.server = flickrPhoto.server
        photo.secret = flickrPhoto.secret
        photo.farm = Int64(flickrPhoto.farm)
        photo.title = flickrPhoto.title
        photo.tags = flickrPhoto.tags
        photo.dateTaken = flickrPhoto.dateTaken ?? Date(timeIntervalSinceReferenceDate: 0.0)
        
        // Work out the aspect ratio if we have it
        if let image = flickrPhoto.imageZ {
            photo.aspectRatio = image.size.width / image.size.height
        } else {
            photo.aspectRatio = 1.0
        }
        
        // Set the correct place
        if let placeId = flickrPhoto.placeId {
            photo.place = Place.get(with: placeId, in: context)
        } else {
            photo.place = nil
        }
        
        // Return the photo
        return photo
    }
    
    /// Finds or creates a Photo with the given identifier in the specified context
    ///
    /// - Parameters:
    ///   - identifier: The identfier of the Photo to look for or create
    ///   - context: The context to search
    /// - Returns: The newly created photo or the first result with the specified identifier
    static func get(with identifier: String, in context: NSManagedObjectContext) -> Photo {
        
        // A fetch request to query this specific photo
        let request: NSFetchRequest<Photo> = Photo.fetchRequest()
        request.fetchLimit = 1
        request.predicate = NSPredicate(format: "identifier == %@", identifier)
        
        // Fetch or create a new Photo to represent this object
        if let result = try? context.fetch(request), let photo = result.first {
            return photo
        } else {
            let photo = Photo(context: context)
            photo.identifier = identifier
            return photo
        }
    }
    
    /// Builds and returns the appropriate image url based on https://www.flickr.com/services/api/misc.urls.html
    var imageURL: URL? {
        
        // If the device scale is > 2.0 then we use "b" large, otherwise we use "z" medium
        let size = UIScreen.main.nativeScale > 2.0 ? "b" : "z"
        
        // Make sure all the fields on the image were present
        guard let server = server, let identifier = identifier, let secret = secret else {
            return nil
        }
        
        // Return the URL
        let string = "https://farm\(farm).staticflickr.com/\(server)/\(identifier)_\(secret)_\(size).jpg"
        return URL(string: string)
    }
    
    /// Returns the Date in a nicer swift format rather than using NSDate or TimeInterval
    var dateTaken: Date {
        get {
            return Date(timeIntervalSinceReferenceDate: dateTakenValue)
        }
        set {
            dateTakenValue = newValue.timeIntervalSinceReferenceDate
        }
    }
}
