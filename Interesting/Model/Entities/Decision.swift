//
//  Decision.swift
//  Interesting
//
//  Created by Liam Nichols on 12/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import CoreData

extension Decision {
    
    /// Returns the Date in a nicer swift format rather than using NSDate or TimeInterval
    var date: Date {
        get {
            return Date(timeIntervalSinceReferenceDate: dateValue)
        }
        set {
            dateValue = newValue.timeIntervalSinceReferenceDate
        }
    }
}
