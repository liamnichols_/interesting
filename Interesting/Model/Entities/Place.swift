//
//  Place.swift
//  Interesting
//
//  Created by Liam Nichols on 12/02/2017.
//  Copyright © 2017 Liam Nichols. All rights reserved.
//

import CoreData

extension Place {
    
    /// Finds or creates a Place with the given identifier in the specified context
    ///
    /// - Parameters:
    ///   - identifier: The identfier of the Place to look for or create
    ///   - context: The context to search
    /// - Returns: The newly created place or the first result with the specified identifier
    static func get(with identifier: String, in context: NSManagedObjectContext) -> Place {
        
        // A fetch request to query this specific photo
        let request: NSFetchRequest<Place> = Place.fetchRequest()
        request.fetchLimit = 1
        request.predicate = NSPredicate(format: "identifier == %@", identifier)
        
        // Fetch or create a new Photo to represent this object
        if let result = try? context.fetch(request), let place = result.first {
            return place
        } else {
            let place = Place(context: context)
            place.identifier = identifier
            return place
        }
    }
    
}
