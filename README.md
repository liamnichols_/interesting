# Interesting

### Description

Interesting is a simple iOS application that fetches the [interesting](https://www.flickr.com/explore/) photos of the day from Flickr. 

Information about each photo is then parsed into a database managed by Core Data where the user interface then presents each photo to the user one by one. The user then has the ability to like or dislike each photo.  

Photos the user likes are then stored and the user can browse back through their photos of interest via the "Explore" screen accessed from the nav bar button in the app. The user is able to then remove the photos, save them or view them in full screen (both portrait or landscape).  
  
Likes can be triggered by tapping the ❤️ button or double tapping the image and dislikes are trigged using the ❌ button or simply swiping from the bottom to the top to reveal the next image.
  
### Technical

The project was written using Swift 3 in Xcode 8.2 and can be built for testing without any other dependencies (Carthage is required for proper device archives).

Requires an iPhone running iOS 10.2.x


### Demo

![Kiku](demo.gif)